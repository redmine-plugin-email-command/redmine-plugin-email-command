# Copyright (C) 2019  Sutou Kouhei <kou@clear-code.com>
# Copyright (C) 2019  Shimadzu Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Redmine::Plugin.register :email_command do
  name "Email command plugin"
  author "Sutou Kouhei, Shimadzu Corporation"
  description "This is a Redmine plugin to provides a command that sends email with Redmine configuration"
  version "1.0.1"
  url "https://gitlab.com/redmine-plugin-email-command/redmine-plugin-email-command"
  author_url "https://gitlab.com/redmine-plugin-email-command"
  directory __dir__
end
